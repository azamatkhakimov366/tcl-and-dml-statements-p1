insert into film(film_id, title, description, release_year, language_id, original_language_id, rental_duration,
                 rental_rate, length, replacement_cost, special_features)
values (10011, 'The Lord of the Rings: The Return of the King',
        'The hobbit, Sméagol, is fishing with his cousin Déagol, who discovers the One Ring in the river. Sméagol''s mind is ensnared by the Ring, and he kills his cousin for it. Increasingly corrupted physically and mentally, he retreats into the Misty Mountains and becomes known as Gollum.',
        '2003', 1, null, 14, 4.99, 120, 12.8, '{"epic","fantasy","adventure film"}')